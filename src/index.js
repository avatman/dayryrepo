import React from 'react';
import ReactDOM from 'react-dom';
import { createStore } from 'redux'
import { Provider } from 'react-redux';
import {rootReducer} from './store/reducers'
import App from './components/app/App';
import './index.css';

export const ACTION_ADD_ITEM = 'ACTION_ADD_ITEM';
export const ACTION_REMOVE_ITEM = 'ACTION_REMOVE_ITEM';
export const ACTION_MAKE_ACTIVE = 'ACTION_MAKE_ACTIVE';

const store = createStore(rootReducer);

ReactDOM.render(<Provider store={store}>
                    <App />
                </Provider>, document.getElementById('root'));
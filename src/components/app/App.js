import React from 'react'
import Items from '../items/Items'
import { Comments } from '../comments/Comments'
import { connect } from 'react-redux'
import './App.css'

class App extends React.Component {



    render() {
        return (
            <React.Fragment>
                <div className='container'>
                    <div className='sideLine'>
                        <h1>ToDo App</h1>
                        <h6>Test Task</h6>
                    </div>
                        <div>
                            <Items/>
                        </div>
                        <div>
                            <Comments/>
                        </div>
                </div>
            </React.Fragment>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        items: state.items
    }
}

export default connect(mapStateToProps)(App);
import React from 'react'
import './Items.css'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { addItem, removeItem, makeActive } from '../../store/actions'

class Items extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            input: '',
            delete: true,
        };
    }

    handleChange(event) {
        this.setState({input: event.target.value});
    }

    handleSubmit(props) {
        props.addItem({input: this.state.input, active: false, class: 'item', comments: []});
        this.setState({input: ''});
    }

    handleDelete(id) {
        this.props.items.splice(id, 1);
        this.props.removeItem(this.props.items);
        this.setState({delete: !this.state.delete});
    }

    handleLi(id) {
        let array = this.props.items;
        array.forEach((el) => {el.active = 'false'; el.class = 'item'})
        array[id].class = 'item-active';
        array[id].active = true;
        this.props.makeActive(array);
        this.setState({delete: !this.state.delete});
    }

    render () {
        const items = this.props.items.map( (input, index) => {
            return (
                <li key={index} onClick={() => this.handleLi(index)} className={input.class} >
                    <span>{input.input}</span>
                    <span className='delete-side'>
                        <div className='comment-counter'>0</div>
                        <button onClick={() => this.handleDelete(index)}>Delete</button>
                    </span>
                </li>
            );
        });

        return (
            <div className='items-list'>
                <p>Items</p>
                <input type='text' value={this.state.input} placeholder='Type name here...' onChange={this.handleChange.bind(this)}/>   
                <button onClick={() => this.handleSubmit(this.props)}>Add New</button>
                <ul>
                    {items}
                </ul>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        items: state.items,
    }
};
const mapDispatchToProps = (dispatch) => {
    return {
        addItem: bindActionCreators(addItem, dispatch),
        removeItem: bindActionCreators(removeItem, dispatch),
        makeActive: bindActionCreators(makeActive, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Items);


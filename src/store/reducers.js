import { ACTION_ADD_ITEM,
         ACTION_REMOVE_ITEM,
         ACTION_MAKE_ACTIVE } from '../index';



const initialState = {
    items: [],
};

export const rootReducer = (state = initialState, action) => {
    switch (action.type) {
        
        case ACTION_ADD_ITEM: 
            return {
                ...state,
                items: [...state.items, action.payload],
            };

        case ACTION_REMOVE_ITEM:
            return {
                ...state,
                items: action.payload
            };

        case ACTION_MAKE_ACTIVE:
            return {
                ...state,
                items: action.payload
            };    

        default: 
            return state;
    };    
};
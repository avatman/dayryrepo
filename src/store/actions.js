import { ACTION_ADD_ITEM, 
         ACTION_REMOVE_ITEM, 
         ACTION_MAKE_ACTIVE } from '../index'

export const addItem = (newItem) => {
    return {
        type: ACTION_ADD_ITEM,
        payload: newItem,
    };
}
export const removeItem = (newArray) => {
    return {
        type: ACTION_REMOVE_ITEM,
        payload: newArray,
    };
}
export const makeActive = (newArray) => {
    return {
        type: ACTION_MAKE_ACTIVE,
        payload: newArray,
    };
}